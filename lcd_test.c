/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include "includes.h"
#include "Adafruit_LCD.h"
#include "alt_types.h"
#include <stdbool.h>
#include <stdlib.h>

/* LCD Defines */
#define LCD_RESET 	LCD_RST_BASE	// Location of SPI reset pin
#define lcd_width	800
#define lcd_height	480

int main(void)
{
	* (long*)LCD_RST_BASE=0x00;
	delay(1000);

	/* LCD Testing */
	Adafruit_LCD *lcd;
	Adafruit_LCD_construct(lcd, lcd_width, lcd_height);

	* (long*)LCD_RST_BASE=0x01;
	delay(1000);

	/* Error Check */
	if (readReg(0) != 0x75){
		while(1){
			printf("ERROR: CANT SEE BOARD\n");
			readReg(0);
			delay(100);
		}
	}

	/* Initialize LCD */
	initialize(lcd);
	displayOn(true);
	GPIOX(true);      							// Enable TFT - display enable tied to GPIOX
	PWM1config(true, RA8875_PWM_CLK_DIV1024); 	// PWM output for backlight
	PWM1out(255);								// Set to highest brightness
	fillScreen(lcd, RA8875_BLUE);				// Set background blue


	/* Play with PWM (Controls the brightness of the screen)  */
	alt_u8 i;
	
	/* Dim the display  */
	for (i=255 ; i!=0 ; i-=5 ){
		PWM1out(i);
		delay(10);
	}
	/* Un-dim the display  */
	for (i=0; i!=255; i+=5 ){
		PWM1out(i);
		delay(10);
	}
	PWM1out(255);

	/* Fill Screen Tests */
	printf("Testing fillScreen()\n");
	fillScreen(lcd, RA8875_RED);
	delay(500);
	fillScreen(lcd, RA8875_YELLOW);
	delay(500);
	fillScreen(lcd, RA8875_ORANGE);
	delay(500);
	fillScreen(lcd, RA8875_GREEN);
	delay(500);
	fillScreen(lcd, RA8875_TEAL);
	delay(500);
	fillScreen(lcd, RA8875_INDIGO1);
	delay(500);
	fillScreen(lcd, RA8875_BLACK);


	/* Drawing Shape Tests */
	printf("Testing shape functions\n");
	drawCircle(400, 240, 250, RA8875_NAVY);
	fillCircle(400, 240, 249, RA8875_LIGHTORANGE);
	delay(500);

	fillRect(11, 11, 398, 198, RA8875_CYAN);
	drawRect(10, 10, 400, 200, RA8875_YELLOW);
	delay(500);
	drawPixel(10,10,RA8875_PINK);
	drawPixel(11,11,RA8875_BLACK);
	delay(500);
	drawLine(10, 10, 790, 470, RA8875_RED);
	delay(500);
	drawTriangle(350, 15, 600, 300, 780, 280, RA8875_YELLOW);
	fillTriangle(350, 16, 599, 299, 781, 279, RA8875_ORANGE);
	delay(500);
	drawEllipse(500, 250, 100, 40, RA8875_BLACK);
	fillEllipse(500, 250, 98, 38, RA8875_TEAL);
	delay(1000);
	
	/* Clear display by filling screen */
	fillScreen(lcd, RA8875_WHITE);


	/* Test text functions */
	printf("Testing text mode\n");
	textMode();

	/* Render some text! */
	char string[15] = "Hello, World! ";
	textSetCursor(10,10);
	textEnlarge(lcd,3);
	textColor(RA8875_BLACK, RA8875_PINK);
	textWrite(lcd, string,15);
	textColor(RA8875_WHITE, RA8875_RED);
	textWrite(lcd, string,15);
	textColor(RA8875_ORCHID, RA8875_DARKCYAN);
	textWrite(lcd, string,15);

	textSetCursor(10,150);
	textEnlarge(lcd,2);
	textColor(RA8875_BLACK, RA8875_TEAL);
	textWrite(lcd, string,15);
	textColor(RA8875_WHITE, RA8875_DARKPURPLE);
	textWrite(lcd, string,15);

	textSetCursor(10,240);
	textEnlarge(lcd,1);
	textColor(RA8875_BLUE, RA8875_YELLOW);
	textWrite(lcd, string, 15);
	textColor(RA8875_ORANGE, RA8875_INDIGO2);
	textWrite(lcd, string, 15);

	textSetCursor(10,310);
	textEnlarge(lcd,0);
	textColor(RA8875_YELLOW, RA8875_DARKCYAN);
	textWrite(lcd, string, 15);
	textColor(RA8875_DARKGREEN, RA8875_ORCHID);
	textWrite(lcd, string, 15);
	delay(2000);

	/* Clear display by filling screen */
	graphicsMode();
	fillScreen(lcd, RA8875_WHITE);
	textMode();
	char touchstring[24] = "Try Touchscreen Drawing";
	textSetCursor(10,10);
	textEnlarge(lcd,3);
	textColor(RA8875_DARKPURPLE, RA8875_WHITE);
	textWrite(lcd, touchstring, 24);

	/* Touch screen functions */
	touchEnable(true);
	float xScale = 1024.0F/width(lcd);
	float yScale = 1024.0F/height(lcd);
	alt_u16 tx, ty;

	while(1){
		if (touched()){
			touchRead(&tx, &ty);
			fillCircle((alt_u16)(tx/xScale), (alt_u16)(ty/yScale), 4, RA8875_DARKGREEN);
		}
		delay(20);
	}

	return 0;
}





/******************************************************************************
 *                                                                             *
 * License Agreement                                                           *
 *                                                                             *
 * Copyright (c) 2004 Altera Corporation, San Jose, California, USA.           *
 * All rights reserved.                                                        *
 *                                                                             *
 * Permission is hereby granted, free of charge, to any person obtaining a     *
 * copy of this software and associated documentation files (the "Software"),  *
 * to deal in the Software without restriction, including without limitation   *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,    *
 * and/or sell copies of the Software, and to permit persons to whom the       *
 * Software is furnished to do so, subject to the following conditions:        *
 *                                                                             *
 * The above copyright notice and this permission notice shall be included in  *
 * all copies or substantial portions of the Software.                         *
 *                                                                             *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE *
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER      *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         *
 * DEALINGS IN THE SOFTWARE.                                                   *
 *                                                                             *
 * This agreement shall be governed in all respects by the laws of the State   *
 * of California and by the laws of the United States of America.              *
 * Altera does not recommend, suggest or require that this reference design    *
 * file be used in conjunction or combination with any other product.          *
 ******************************************************************************/
